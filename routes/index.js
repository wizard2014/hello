
/*
 * GET home page.
 */

var init = require("../models/init");

exports.index = function(req, res){
  res.render('index', { title: init.getTasks().title });
};